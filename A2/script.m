clear all, close all
rng(12042209)

data = matfile('FaceData');
[id, face_no] = size(data, 'FaceData');

image = data.FaceData(1,1);
image_size = size(image.Image);
pixel_count = image_size(1) * image_size(2);

% 200 colums, each for one face of one participant, each column hold one
% vectorized image
X_tr = zeros(pixel_count, id * face_no / 2);
X_te = zeros(pixel_count, id * face_no / 2);

% Create training IDs
tr_ids = randperm(id, id/2);

% Track column in tr and te
tr_col = 1;
te_col = 1;

% vectorise images in Xtr and Xte
for x = 1:id
    for y = 1:face_no
        % Take image corresponding to x & y
        img = data.FaceData(x, y);
        % Vectorize it
        vector = reshape(double(img.Image)/255, pixel_count, 1);
        
        % For everz pixel in the vector
        for p = 1: size(vector)
            % If the id (x) is in the training set add it there
            if any(tr_ids(:) == x)
                X_tr(p, tr_col) = vector(p);
            else
                X_te(p, te_col) = vector(p);
            end
        end
        
        % update column tracker
        if any(tr_ids(:) == x)
            tr_col = tr_col + 1;
        else
            te_col = te_col + 1;
        end
    end
end

phi_0 = mean(X_tr, 2);
X_0 = X_tr - phi_0;
covariances = cov(X_0.');
[eigen_vectors, eigen_values] = eig(covariances);


%title('Variance of the data represented in first m principal components');
%xlabel('m1');
%ylabel('v(m1)');

%plot mean face 😠
avg_face = reshape(phi_0,56,46);
imagesc(avg_face);
title("Mean face");
colormap(gray);

%v(m)
eigen_values_vector = diag(eigen_values);
eigen_values_vector_sorted = sort(eigen_values_vector, 'descend');
eigen_values_sum = sum(eigen_values_vector);
eigen_values_inc_sum = 0;
v = zeros(2, 200);
for m = 1:200
    eigen_values_inc_sum = eigen_values_inc_sum + eigen_values_vector_sorted(m);
    v(1,m) = m;
    v(2,m) = eigen_values_inc_sum/eigen_values_sum;
end
figure
plot(v(1,:),v(2,:));
title("variance of the data represented in the first m principal components");
xlabel("m");
ylabel("v(m)");
ylim([0 1]);

%set m here
m = 100;

%plot eigen faces for m
phi_m = get_eigenfaces(m, eigen_values, eigen_vectors);
draw_faces(phi_m, phi_0);

test_pca(m, X_te, phi_0, eigen_values, eigen_vectors);

function out = get_eigenfaces(m, eigen_values, eigen_vectors)
eigen_val = zeros(1, 2576);
for i = 1: 2576
    eigen_val(i) = eigen_values(i, i);
end

[~, indexes] = maxk(eigen_val, m);
out = eigen_vectors(:,indexes);
end

function draw_faces(max_eigenvecs, phi_0)
% Average face of training data, based off phi_0
avg_face = reshape(phi_0,56,46);
figure
% Draw 10 eigen faces
for i=1:10
    my_face = reshape(max_eigenvecs(:,i),56,46);
    subplot(2,5,i);
    imagesc(avg_face + my_face);
    title("Eigenface " + i);
    colormap(gray);
end
end

function out = FMR(t, data)
% If they different ID, but score is below threshold
out = 0;
count = 0;
for x = 1:200
    for y = x + 1:200
        if fix((x-1)/10) ~= fix((y-1)/10)
            if data(x, y) < t
                out = out + 1;
            end
            count = count + 1;
        end
    end
end
out = out / count;
end

function out = TMR(t, data)
% If they same ID, but score is below threshold
out = 0;
count = 0;
for x = 1:200
    for y = x + 1:200
        if fix((x-1)/10) == fix((y-1)/10)
            if data(x, y) < t
                out = out + 1;
            end
            count = count + 1;
        end
    end
end
out = out / count;
end

function out = FNMR(t, data)
out = 1.0 - TMR(t, data);
end

%step 4-5 for m
function eer_threshold = test_pca(m, X_te, phi_0, eigen_values, eigen_vectors)
phi_m = get_eigenfaces(m, eigen_values, eigen_vectors);
pca = [];
for i = 1: 200
    a = transpose(phi_m) * (X_te(:, i) - phi_0);
    pca = [pca, a];
end

S = zeros(200, 200);
for i = 1: 200
    for j = 1: 200
        S(i, j) = norm(pca(:,i) - pca(:,j));
    end
end

threshold_count = 500;
roc_data = zeros(5, threshold_count);
for t=1:threshold_count
    roc_data(1, t) = (t-1) * 16/threshold_count;
    roc_data(2, t) = FMR((t-1) * 16/threshold_count, S);
    roc_data(3, t) = TMR((t-1) * 16/threshold_count, S);
    roc_data(4, t) = FNMR((t-1) * 16/threshold_count, S);
    roc_data(5, t) = abs(roc_data(2, t) - roc_data(4, t));
end

[~, min_index] = min(roc_data(5,:));

eer_threshold = roc_data(1, min_index);

figure
plot(roc_data(2, :), roc_data(3, :));
title("ROC for m = " + m + ", EER at threshold " + eer_threshold);
xlabel("FMR(t)");
ylabel("TMR(t)");
end
