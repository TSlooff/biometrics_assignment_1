def read_data():
    import pandas as pd
    scores = pd.read_csv('data/scorematrix.txt', sep=" ", header = None)
    ids = pd.read_csv('data/id.txt', sep=" ", header=None)

    # For some reason there is an additional column of NaNs
    scores = scores.iloc[:,:943]
    ids = ids.iloc[:,:943]
    
    # Set the IDs
    scores.columns = ids.loc[0].values
    scores.index = ids.loc[0].values

    del(ids)
    return scores

def read_normal_data():
    import pandas as pd
    scores = pd.read_csv('data/normal_data.csv')
    ids = pd.read_csv('data/id.txt', sep=" ", header=None)
    ids = ids.iloc[:,:943]

    scores.columns = ids.loc[0].values
    scores.index = ids.loc[0].values

    del(ids)
    return scores

